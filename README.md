# C.V. dynamique personnalisé

C.V. interactif en VueJS/Nuxt récupéré [ici](https://github.com/ivangreve/nuxt-resume) et adapté pour être publié sur GitLab Pages.

Le lien du C.V. est accessible [ici](https://maximecharpentierdev.gitlab.io/nuxt-resume/).

# Technologies et outils mis en oeuvre 🔧🦾

* VueJs
* Nuxt
* i18n
* ESLint + Prettier
* Bootstrap
* CI/CD GitLab
